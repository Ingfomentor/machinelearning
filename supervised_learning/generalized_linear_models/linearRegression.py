#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  linearRegression.py
#  
#  Copyright 2018 Angel Oswaldo Vázquez Patiño <angel.vazquezp@ucuenca.edu.ec>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

# Based in the implementation of https://towardsdatascience.com/linear-regression-in-6-lines-of-python-5e1d0cd05b8d

from sklearn.datasets import make_regression # To generate a random regression problem
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt # To visualize

def main(args):
	## Generation of data set
	X, Y = make_regression(n_samples=100, n_features=2, n_informative=2, n_targets=1, noise=0.5, random_state=10)
	X = X[:, 0]
	X = X.reshape(-1, 1) # values converts it into a numpy array
	Y = Y.reshape(-1, 1) # -1 means that calculate the dimension of rows, but have 1 column

	linear_regressor = LinearRegression()  # create object for the class
	linear_regressor.fit(X, Y)  # perform linear regression
	Y_pred = linear_regressor.predict(X) # make predictions
	
	plt.title('Linear Regression')
	plt.xlabel('Predictor (X)')
	plt.ylabel('Predictand (Y)')
	plt.scatter(X, Y)
	plt.plot(X, Y_pred, color='red')
	plt.show()
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
